#!/usr/bin/env python

import os
import sys
import re
import subprocess


# Run a shell command and return its return status.
def run( command ):
    pop = subprocess.Popen( command, shell = True, stdout = subprocess.PIPE )
    log( pop.communicate()[ 0 ].strip() )

    return pop.returncode


# Print data to screen and to log file.
def log( data ):
    path = 'log'
    name = '%s.log' % instance
    filepath = '%s/%s' % ( path, name )

    # Create the destination log file directory if it does not exist.
    if not os.path.exists( path ):
        os.mkdir( path )

    # Print data to screen and to log file.
    print '\x1b[96m%s\x1b[0m' % data
    with open( filepath, 'aw' ) as logfile:
        logfile.write( '\x1b[96m%s\x1b[0m\n' % data )


# Get the number of keys registered to openstack and the name of the key to be used.
def keyinfo():
    keypairs = os.popen( 'openstack keypair list -f csv -c Name --quote none | sed \'1,1d\'' ).read().strip().split()

    # Find keys currently registered with openstack.
    keyname = ''
    if len( keypairs ) == 1:
        keyname = keypairs[ 0 ]
    elif len( keypairs ) > 1:
        choices = ', '.join( keypairs )
        while keyname not in keypairs:
            sys.stdout.write( '\x1b[1;93mChoose a key to use among (%s) [%s]: \x1b[0m' % ( choices, keypairs[ 0 ] ) )
            sys.stdout.flush()
            keyname = sys.stdin.readline().strip() or keypairs[ 0 ]
    else:
        keyname = 'cern'

    return keyname, len( keypairs )



# Create an ssh keypair if it does not exist already.
def createkey( info ):
    keyname, nkeys = info

    # If no key has yet ben registered with openstack, register one.
    if nkeys == 0:
        # Generate if if necessary.
        keyfile = '~/.ssh/id_rsa_test'
        if not os.path.exists( os.path.expanduser( keyfile ) ):
            log( '\x1b[93mCreating a new ssh key in \x1b[1m%s\x1b[21m.\x1b[0m' % keyfile )
            run( 'ssh-keygen -b 4096 -N \'\' -f %s' % keyfile )

        # Register the key with openstack.
        log( "\x1b[93mAdding public key \x1b[1m%s.pub\x1b[21m to openstack with name \x1b[1m%s\x1b[21m.\x1b[0m" % ( keyfile, keyname ) )
        run( 'openstack keypair create --public-key %s.pub %s' % ( keyfile, keyname ) )
    else:
        log( '\x1b[96mUsing existing key \x1b[1m%s\x1b[21m.\x1b[0m' % keyname )


# Create the requested instance on openstack.
def createinstance( itype, instance, info ):
    keyname, nkeys = info

    # For servers, use a small flavor (1 vcpu). For agents use a large flavor (4 vcpus).
    flavor = 'm2.small' if itype == 'server' else 'm2.large'

    # If it already exists, do not attempt to create it.
    instances = os.popen( 'openstack server list -f csv -c Name -c Status --quote none | sed \'1,1d\'' ).read().strip().split()
    instances = dict( [ tuple( inst.split( ',' ) ) for inst in instances ] )
    if instance in instances:
        status = instances.get( instance ).lower()
        log( "\x1b[95mInstance \x1b[1m%s\x1b[21m already exists and is \x1b[1m%s\x1b[21m.\x1b[0m" % ( instance, status ) )
        return 11

    # Find the image id of the newest image of the requested operating system.
    hw        = os.uname()[ 4 ]   # Hardware id.
    imagename = '%s - %s' % ( syst.upper(), hw )
    command   = 'openstack image list -f csv -c ID -c Name --quote none | sed \'1,1d\' | grep "%s" | head -n 1' % imagename
    imageid   = os.popen( command ).read().strip().split( ',' )[ 0 ]

    # Create the new instance.
    log( "\x1b[93mCreating a new %s instance named \x1b[1m%s\x1b[21m.\x1b[0m" % ( itype, instance ) )
    status = run( 'openstack server create --key-name %s --flavor %s --user-data cfg/install%s --image %s %s' % ( keyname, flavor, itype, imageid, instance ) )

    if status:
        sys.exit( status )



# Read the instance name with a default option given.
def readinstance( itype ):
    default = "%s-%s" % ( itype, os.environ.get( 'USER' ) )
    sys.stdout.write( '\x1b[1;93mPlease provide the name of your instance [%s]: \x1b[0m' % default )
    sys.stdout.flush()
    return sys.stdin.readline().strip() or default


itype    = sys.argv[ 1 ] if len( sys.argv ) > 1 else None
instance = sys.argv[ 2 ] if len( sys.argv ) > 2 else None
syst     = sys.argv[ 3 ] if len( sys.argv ) > 3 else 'slc6'

if itype != 'server' and itype != 'agent':
    print '\x1b[95mInstance type can only be \'server\' or \'agent\', not \x1b[1m%s\x1b[21m.\x1b[0m' % itype
    sys.exit( 10 )

# Convert spaces to dashes in the instance name.
instance = instance or readinstance( itype )
instance = re.sub( ' +', '-', instance )

info = keyinfo()
createkey     (                  info )
createinstance( itype, instance, info )

